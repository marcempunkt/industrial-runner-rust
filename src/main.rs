use bevy::prelude::*;

const WINDOW_WIDTH: f32 = 1200.0;
const WINDOW_HEIGHT: f32 = 720.0;
const PLAYER_WIDTH: f32 = 10.0;
const PLAYER_HEIGHT: f32 = 20.0;
const PLAYER_SPEED: f32 = 10.0;

/* Bevy's default pluings don't include a camera though, so let's insert
 * a 2d camera which we'll set up by creating our first system */
fn setup_camera(mut commands: Commands) {
    /* Commands is used to queue up commands to mutuate the world and resources
     * In this case, we're spawning a new entity, with 2D camera components */
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
}

// -----------------------------------------------------------------------------------------------
#[derive(Component)]
struct Player; // components often don't need any state of their own.

const BLACK: Color = Color::rgb(0.0, 0.0, 0.0);

fn spawn_player(mut commands: Commands) {
    commands.spawn_bundle(SpriteBundle {
	sprite: Sprite {
	    color: BLACK,
	    ..Default::default()
	},
	transform: Transform {
	    scale: Vec3::new(PLAYER_WIDTH, PLAYER_HEIGHT, 10.0),
	    translation: Vec3::new(0., -WINDOW_HEIGHT/2. + PLAYER_HEIGHT/2., 0.),
	    ..Default::default()
	},
	..Default::default()
	// insert ist welche custom components man dranhängen möchte
    }).insert(Player); 
}

// -----------------------------------------------------------------------------------------------
#[derive(Component)]
struct Platform;

fn spawn_platforms(mut commands: Commands) {
    commands.spawn_bundle(SpriteBundle {
	sprite: Sprite {
	    color: BLACK,
	    ..Default::default()
	},
	transform: Transform {
	    scale: Vec3::new(400.0, 100.0, 10.0),
	    translation: Vec3::new(0., 0., 0.),
	    ..Default::default()
	},
	..Default::default()
    }).insert(Platform);
}

// -----------------------------------------------------------------------------------------------
fn gravity(mut query: Query<(&Player, &mut Transform)>) {
    let (_player, mut transform) = query.single_mut();
    transform.translation.y -= 1.;
    // query.iter_mut().for_each(|(_player, mut transform)| transform.translation.y -= 1.);
}

fn screen_boundary(mut query: Query<(&Player, &mut Transform)>) {
    let (_player, mut transform) = query.single_mut();

    let screen_bottom: f32 = -WINDOW_HEIGHT/2.;
    let screen_top: f32 = WINDOW_HEIGHT/2.;
    let screen_right: f32 = WINDOW_WIDTH/2.;
    let screen_left: f32 = -WINDOW_WIDTH/2.;
// TODO hier ist ein Bug
    if transform.translation.y < screen_bottom + PLAYER_HEIGHT/2. {
	/* BOTTOM */
	transform.translation.y = screen_bottom + PLAYER_HEIGHT/2.;
    } else if transform.translation.y > screen_top - PLAYER_HEIGHT/2. {
	/* TOP */
	transform.translation.y = screen_top - PLAYER_HEIGHT/2.;
    } else if transform.translation.x < screen_left + PLAYER_WIDTH/2. {
	/* LEFT */
	transform.translation.x = screen_left + PLAYER_WIDTH/2.
    } else if transform.translation.x > screen_right - PLAYER_WIDTH/2. {
	/* RIGHT */
	transform.translation.x = screen_right - PLAYER_WIDTH/2.
    }
}
// -----------------------------------------------------------------------------------------------
fn player_keylistener(keyboard_input: Res<Input<KeyCode>>, mut query: Query<&mut Transform, With<Player>>) {
    let mut transform = query.single_mut();
    // let (_player, mut transform) = query.single_mut(); this doesn't work with this kind of query anymore
    if keyboard_input.pressed(KeyCode::Left) {
	transform.translation.x -= PLAYER_SPEED;
    } else if keyboard_input.pressed(KeyCode::Right) {
	transform.translation.x += PLAYER_SPEED;
    } else if keyboard_input.pressed(KeyCode::Space) {
	transform.translation.y += PLAYER_SPEED;
    } else if keyboard_input.pressed(KeyCode::Down) {
	transform.translation.y -= PLAYER_SPEED;
    }
}
// -----------------------------------------------------------------------------------------------

// fn ground(mut)

fn main() {
    App::new()
	.insert_resource(ClearColor(Color::rgb(0.71, 0.17, 0.3)))
	.insert_resource(WindowDescriptor{
	    width: WINDOW_WIDTH,
	    height: WINDOW_HEIGHT,
	    title: "industrial runner".to_string(),
	    ..Default::default()
	})
	.add_startup_system(setup_camera)
	// Das alles könnte ein Plugin sein!
	.add_startup_system(spawn_player)
	.add_system(gravity)
	.add_system(screen_boundary)
	.add_system(player_keylistener)
	// PlayerPlugin ENDE
	// Platform Plugin Start
	.add_system(spawn_platforms)
	// ENDE
	.add_plugins(DefaultPlugins).run();
}
